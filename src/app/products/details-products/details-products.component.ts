import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../shared/data.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-details-products',
  templateUrl: './details-products.component.html',
  styleUrls: ['./details-products.component.css']
})
export class DetailsProductsComponent {
  constructor(private route:ActivatedRoute, private dataSvc: DataService, private location: Location ) {}


  public product: any = {}

  ngOnInit(): void{
    const productID = this.route.snapshot.paramMap.get('id');
    [this.product]= this.dataSvc.getProductById(productID);
  }

  onGoBack(): void{
    this.location.back()
  }
}
