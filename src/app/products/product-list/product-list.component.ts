import { Component } from '@angular/core';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent {
  public products: any[] = [];

  constructor(private dataSvc: DataService){}

  ngOnInit(): void{
    this.products = this.dataSvc.getAllProducts();
  }

}
