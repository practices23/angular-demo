import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { NoFoundComponent } from './no-found/no-found.component';
import { DetailsProductsComponent } from './products/details-products/details-products.component';

const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'products', component: ProductListComponent},
  {path:'product/:id', component: DetailsProductsComponent},
  {path:'**', component: NoFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
