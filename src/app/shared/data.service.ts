import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private products = [
    {
      id:1,
      name:'Egg',
      category:'Food',
      description:'Lorem egg',
      price:1
    },
    {
      id:2,
      name:'Coca Cola',
      category:'Drink',
      description:'Lorem Drink',
      price: 10
    },
    {
      id:2,
      name:'Chokis',
      category:'Cookies',
      description:'Lorem Cookies',
      price: 5
    }
  ];

  getAllProducts(){
    return this.products
  }

  getProductById(id: any){

    return this.products.filter(product =>  product.id == id )

  }

}
